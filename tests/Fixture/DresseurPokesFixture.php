<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DresseurPokesFixture
 */
class DresseurPokesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'dresseur_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'poke_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'poke_id' => ['type' => 'index', 'columns' => ['poke_id'], 'length' => []],
            'dresseur_id' => ['type' => 'index', 'columns' => ['dresseur_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'dresseur_pokes_ibfk_1' => ['type' => 'foreign', 'columns' => ['poke_id'], 'references' => ['pokes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dresseur_pokes_ibfk_2' => ['type' => 'foreign', 'columns' => ['dresseur_id'], 'references' => ['dresseurs', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'dresseur_id' => 1,
                'poke_id' => 1,
                'created' => '2019-12-11 06:30:46',
                'modified' => '2019-12-11 06:30:46'
            ],
        ];
        parent::init();
    }
}
