<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Poke'), ['action' => 'edit', $poke->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Poke'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pokes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poke'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pokes view large-9 medium-8 columns content">
    <h3><?= h($poke->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($poke->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($poke->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pokedex Number') ?></th>
            <td><?= $this->Number->format($poke->pokedex_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($poke->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($poke->modified) ?></td>
        </tr>
    </table>
</div>
