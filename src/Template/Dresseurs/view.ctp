<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseur'), ['action' => 'edit', $dresseur->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseur'), ['action' => 'delete', $dresseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseurs view large-9 medium-8 columns content">
    <h3><?= h($dresseur->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nom') ?></th>
            <td><?= h($dresseur->nom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prenom') ?></th>
            <td><?= h($dresseur->prenom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseur->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseur->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseur->modified) ?></td>
        </tr>
    </table>
</div>
