<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPoke $dresseurPoke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseur Poke'), ['action' => 'edit', $dresseurPoke->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseur Poke'), ['action' => 'delete', $dresseurPoke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPoke->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Poke'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pokes'), ['controller' => 'Pokes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poke'), ['controller' => 'Pokes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseurPokes view large-9 medium-8 columns content">
    <h3><?= h($dresseurPoke->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dresseur') ?></th>
            <td><?= $dresseurPoke->has('dresseur') ? $this->Html->link($dresseurPoke->dresseur->prenom, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPoke->dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Poke') ?></th>
            <td><?= $dresseurPoke->has('poke') ? $this->Html->link($dresseurPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseurPoke->poke->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseurPoke->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseurPoke->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseurPoke->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Fav') ?></th>
            <td><?= $dresseurPoke->is_fav ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
