<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DresseurPokemon Entity
 *
 * @property int $id
 * @property string $dresseur_id
 * @property string $pokemon_id
 * @property bool $is_fav
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Dresseur $dresseur
 * @property \App\Model\Entity\Pokemon $pokemon
 */
class DresseurPokemon extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dresseur_id' => true,
        'pokemon_id' => true,
        'is_fav' => true,
        'created' => true,
        'modified' => true,
        'dresseur' => true,
        'pokemon' => true
    ];
}
