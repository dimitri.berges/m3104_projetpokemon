<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            //$fight = $this->Fights->patchEntity($fight, $this->request->getData());
            $formData = $this->request->getData();
            $formData['winner_dresseur_id'] = $this->determine_gagnant($formData);
            if ($formData['winner_dresseur_id']) {
                $fight = $this->Fights->patchEntity($fight, $formData);
                if ($this->Fights->save($fight)) {
                    $this->Flash->success(__('Le combat est lancé !'));
                    $id = $this->Fights->find()->select(['id'])->order(['id' => 'DESC'])->first()['id'];
                    return $this->redirect(['action' => 'view', $id]);
                }
                $this->Flash->error(__('The fight could not be saved. Please, try again.'));
            }
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }

    protected function determine_gagnant($data) {
        $dresseurs = array($data['first_dresseur_id'], $data['second_dresseur_id']);
        $pokemonD1 = $this->Fights->DresseurPokes->find()->select(["poke_id"])->where(["dresseur_id" => $dresseurs[0], "is_fav" => 1])->first()["poke_id"];
        $pokemonD2 = $this->Fights->DresseurPokes->find()->select(["poke_id"])->where(["dresseur_id" => $dresseurs[1], "is_fav" => 1])->first()["poke_id"];
        $res = $this->combat(100, 100);
        if ($res == 2) return null;
        else return $dresseurs[$res];
    }

    protected function combat($pvpok1, $pvpok2) {
        if ($pvpok1 > 0 && $pvpok2 > 0) {
            $pvpok2 -= rand(10, 30);
            if ($pvpok2 < 0) return 0;
            $pvpok1 -= rand(10, 30);
            return $this->combat($pvpok1, $pvpok2);
        } else {
            if ($pvpok1 > 0) return 0;
            elseif ($pvpok2 > 0) return 1;
        }
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
